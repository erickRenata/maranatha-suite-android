package com.maranatha.suite

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main.*
import android.R
import uk.co.chrisjenx.calligraphy.CalligraphyConfig



class MaranathaSuiteApp : Application() {
    override fun onCreate() {
        super.onCreate()

        Hawk.init(applicationContext).build()

//        CalligraphyConfig.initDefault(
//            CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
//                .setFontAttrId(R.attr.)
//                .build()
//        )
    }
}
