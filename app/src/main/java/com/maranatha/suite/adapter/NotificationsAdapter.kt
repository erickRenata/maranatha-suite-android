package com.maranatha.suite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.maranatha.suite.R
import com.maranatha.suite.model.response.Notifications
import kotlinx.android.synthetic.main.item_notifications.view.*

class NotificationsAdapter (val context: Context, val list: MutableList<Notifications>, val listener: (Int) -> Unit) :
        RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notifications, parent, false))


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtMenu.text = list[position].Content
        holder.itemView.setOnClickListener {
            listener.invoke(position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val txtMenu: TextView = view.tv_notifications
    }
}