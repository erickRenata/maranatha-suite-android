package com.maranatha.suite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.maranatha.suite.R
import com.maranatha.suite.model.response.Donation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_donation.view.*

class DonationAdapter (val context: Context, val list: MutableList<Donation>, val listener: (Int) -> Unit) :
        RecyclerView.Adapter<DonationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_donation, parent, false))


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso
                .get()// give it the context
                .load(list[position].Image_Donation) // load the image
                .into(holder.ivDonation)
        holder.tvBudget.text = "Rp ".plus(list[position].Money.toInt().toString())
//        if (a > b) a else b
        holder.tvDate.text = list[position].CreatedAt
        holder.tvUserName.text = if (list[position].User == null) "" else list[position].User.Nama
        holder.tvContent.text = list[position].Description
        holder.itemView.setOnClickListener {
            listener.invoke(position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivDonation: ImageView = view.iv_donation
        val tvBudget: TextView = view.tv_donation_budget
        val tvDate: TextView = view.tv_donation_date
        val tvUserName: TextView = view.tv_donation_user
        val tvContent: TextView = view.tv_donation_content
    }
}