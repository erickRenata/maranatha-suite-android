package com.maranatha.suite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.maranatha.suite.R
import com.maranatha.suite.model.response.Job
import com.maranatha.suite.model.response.JobCategory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_job.view.*

class JobAdapter (val context: Context, val list: MutableList<Job>, val listener: (Int) -> Unit) :
        RecyclerView.Adapter<JobAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_job, parent, false))


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso
                .get()// give it the context
                .load(list[position].Icon_Company) // load the image
                .into(holder.ivJob)
        holder.tvTitle.text = list[position].Position
        holder.tvCompany.text = list[position].Company
        holder.tvLocations.text = list[position].Location
        holder.tvSkillRequirements.text = list[position].Skill_Requirements
        holder.itemView.setOnClickListener {
            listener.invoke(position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivJob: ImageView = view.iv_job
        val tvTitle: TextView = view.tv_job_title
        val tvCompany: TextView = view.tv_job_company
        val tvLocations: TextView = view.tv_job_locations
        val tvSkillRequirements: TextView = view.tv_job_skill_requirements
    }
}