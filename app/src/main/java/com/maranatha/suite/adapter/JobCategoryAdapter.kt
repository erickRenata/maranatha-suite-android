package com.maranatha.suite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.maranatha.suite.R
import com.maranatha.suite.model.response.JobCategory
import com.maranatha.suite.model.response.Notifications
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_job_category.view.*
import kotlinx.android.synthetic.main.item_notifications.view.*

class JobCategoryAdapter (val context: Context, val list: MutableList<JobCategory>, val listener: (Int) -> Unit) :
        RecyclerView.Adapter<JobCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_job_category, parent, false))


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvCategory.text = list[position].CategoriesName
        holder.tvAvailableJobs.text = list[position].Available_Jobs.toString().plus(" job")
        Picasso
                .get()// give it the context
                .load(list[position].Icon_Category) // load the image
                .into(holder.ivJobCategory)
        holder.itemView.setOnClickListener {
            listener.invoke(position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivJobCategory: ImageView = view.iv_job_category
        val tvCategory: TextView = view.tv_category
        val tvAvailableJobs: TextView = view.tv_available_jobs
    }
}