package com.maranatha.suite.adapter

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.maranatha.suite.R
import com.maranatha.suite.model.response.Article
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_article.view.*
import kotlinx.android.synthetic.main.item_notifications.view.*

class ArticleAdapter (val context: Context, val list: MutableList<Article>, val listener: (Int) -> Unit) :
        RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_article, parent, false))


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso
                .get()// give it the context
                .load(list[position].ImageURL) // load the image
                .into(holder.ivArticle)
        holder.tvArticleTitle.text = list[position].Title
        holder.itemView.setOnClickListener {
            listener.invoke(position)
        }
    }

    fun replaceItem(articleList: MutableList<Article>) {
        this.list.clear()
        this.list.addAll(articleList)
        notifyDataSetChanged()
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivArticle: ImageView = view.iv_article
        val tvArticleTitle: TextView = view.tv_article_title
    }
}