package com.maranatha.suite.network

import com.maranatha.suite.model.response.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    /***** User *****/
    @GET("users/Login/{username}/{password}")
    fun postLogin(
        @Path("username") username: String,
        @Path("password") password: String
    ): Observable<User>

    /***** Articles *****/
    @GET("Articles")
    fun getArticleList(

    ): Observable<List<Article>>

    /***** Job *****/
    @GET("JobCategories")
    fun getJobCategories(

    ): Observable<List<JobCategory>>

    @GET("jobs/GetJobByCategoryId/{CategoryId}")
    fun getJobs(
            @Path("CategoryId") username: String
    ): Observable<List<Job>>

    /***** Donation *****/
    @GET("Donations")
    fun getDonations(

    ): Observable<List<Donation>>

    /***** Notifications *****/
    @GET("Notifications")
    fun getNotifications(

    ): Observable<List<Notifications>>
}