package com.maranatha.suite.network

import com.maranatha.suite.constant.Constant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    companion object {
        lateinit var mRetrofit: Retrofit
        fun initRetrofit(): Retrofit {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
            okHttpClient.interceptors().add(loggingInterceptor)
//            okHttpClient.interceptors().add(AppInterceptor())
            okHttpClient.readTimeout(180, TimeUnit.SECONDS)
            okHttpClient.connectTimeout(180, TimeUnit.SECONDS)

            mRetrofit = Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient.build())
                .build()

            return mRetrofit
        }

        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constant.BASE_URL)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }
}