package com.maranatha.suite.network

import com.orhanobut.hawk.Hawk
import okhttp3.Interceptor
import okhttp3.Response

class AppInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        if (Hawk.get<String>("client_id") != null){
            val clientId = Hawk.get<String>("client_id")
            val newRequest = originalRequest.newBuilder()
                .header("client_id", clientId)
                .build()
            return chain.proceed(newRequest)
        }

        return chain.proceed(originalRequest)
    }

}