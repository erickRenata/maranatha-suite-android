package com.maranatha.suite.base

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.maranatha.suite.R
import com.maranatha.suite.network.ApiClient
import com.maranatha.suite.network.ApiService
import com.maranatha.suite.utils.AppSchedulerProvider
import com.maranatha.suite.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper




open class BaseActivity : AppCompatActivity(){

    lateinit var mApiService: ApiService
    lateinit var scheduler: SchedulerProvider
    val disposables = CompositeDisposable()

    lateinit var progressDialog : AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initNetwork()
        initLoadingDialog()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    @SuppressLint("NewApi")
    protected fun initLoadingDialog() {
        val builder = AlertDialog.Builder(this)
        //View view = getLayoutInflater().inflate(R.layout.progress);
        builder.setView(R.layout.layout_progress)
        progressDialog = builder.create()

    }

    protected fun dismissLoading() {
        progressDialog.dismiss()//To change body of created functions use File | Settings | File Templates.
    }

    protected fun showLoading() {
        progressDialog.show()
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /***** API Connection *****/
    fun launch(job: () -> Disposable) {
        disposables.add(job())
    }

    private fun initNetwork() {
        scheduler = AppSchedulerProvider()
        mApiService = ApiClient.initRetrofit().create(ApiService::class.java)
    }
}