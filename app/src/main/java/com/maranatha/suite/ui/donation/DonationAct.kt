package com.maranatha.suite.ui.donation

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.maranatha.suite.R
import com.maranatha.suite.adapter.DonationAdapter
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.Donation
import com.maranatha.suite.utils.with
import kotlinx.android.synthetic.main.activity_donation.*

class DonationAct : BaseActivity() {

    lateinit var donationAdapter: DonationAdapter
    private var donationList: MutableList<Donation> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_donation)

        initOnClickListener()
        getDonations()
    }

    private fun initAdapter() {
        donationAdapter = DonationAdapter(this, donationList, { handleClick(it) })
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = donationAdapter
    }

    /***** Implement Method *****/
    fun handleClick(position: Int) {

    }


    private fun initOnClickListener() {
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }
    /***** API Connection *****/
    fun getDonations() {
        showLoading()
        launch {
            mApiService.getDonations().with(scheduler).subscribe(
                    {
                        dismissLoading()
                        donationList.clear()
                        donationList.addAll(it)
                        initAdapter()
                    },
                    { err ->
                        dismissLoading()
                        Toast.makeText(this, "Oops! Something error ".plus(err), Toast.LENGTH_SHORT).show()
                    })
        }
    }
}
