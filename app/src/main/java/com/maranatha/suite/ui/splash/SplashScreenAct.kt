package com.maranatha.suite.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.maranatha.suite.R
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.User
import com.maranatha.suite.ui.home.HomeAct
import com.maranatha.suite.ui.login.LoginAct
import com.orhanobut.hawk.Hawk

class SplashScreenAct : BaseActivity() {

    private val SPLASH_TIME: Long = 3000 // 3 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            finish()
            if (Hawk.get<User>(User::class.java.name) != null) {
                startActivity(Intent(this, HomeAct::class.java))
            } else {
                startActivity(Intent(this, LoginAct::class.java))
            }
        }, SPLASH_TIME)
    }
}
