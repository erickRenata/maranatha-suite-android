package com.maranatha.suite.ui.forgotpass

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.maranatha.suite.R
import com.maranatha.suite.base.BaseActivity
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordAct : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        btn_send.setOnClickListener {
            hideKeyboard()
            et_email.setText("")
            Toast.makeText(this, "Cek email Anda. Kami telah mengirimkan prosedur pada email Anda.", Toast.LENGTH_SHORT).show()
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }
    }
}
