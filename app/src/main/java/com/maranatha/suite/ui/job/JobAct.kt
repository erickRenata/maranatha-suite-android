package com.maranatha.suite.ui.job

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maranatha.suite.R
import com.maranatha.suite.adapter.JobAdapter
import com.maranatha.suite.adapter.JobCategoryAdapter
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.Article
import com.maranatha.suite.model.response.Job
import com.maranatha.suite.model.response.JobCategory
import com.maranatha.suite.utils.with
import kotlinx.android.synthetic.main.activity_donation.*
import kotlinx.android.synthetic.main.activity_donation.iv_back
import kotlinx.android.synthetic.main.activity_donation.rv_content
import kotlinx.android.synthetic.main.activity_job.*

class JobAct : BaseActivity() {

    lateinit var jobAdapter: JobAdapter
    var jobList: MutableList<Job> = mutableListOf()
    lateinit var jobCategory : JobCategory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)

        jobCategory = intent.getParcelableExtra(JobCategory::class.java.name)
        tv_title.text = jobCategory.CategoriesName
        getJobs()
        initOnClickListener()
    }

    private fun initAdapter() {
        jobAdapter = JobAdapter(this, jobList, { handleClick(it) })
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = jobAdapter
    }

    /***** Implement Method *****/
    fun handleClick(position: Int) {

    }

    private fun initOnClickListener() {
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    /***** API Connection *****/
    fun getJobs() {
        showLoading()
        launch {
            mApiService.getJobs(jobCategory.Id).with(scheduler).subscribe(
                    {
                        dismissLoading()
                        jobList.clear()
                        jobList.addAll(it)
                        initAdapter()
                    },
                    { err ->
                        dismissLoading()
                        Toast.makeText(this, "Oops! Something error ".plus(err), Toast.LENGTH_SHORT).show()
                    })
        }
    }
}
