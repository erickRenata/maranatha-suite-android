package com.maranatha.suite.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.maranatha.suite.R
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.User
import com.maranatha.suite.ui.forgotpass.ForgotPasswordAct
import com.maranatha.suite.ui.home.HomeAct
import com.maranatha.suite.utils.with
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_login.*


class LoginAct : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initOnClickListener()
    }

    private fun initOnClickListener() {
        btn_login.setOnClickListener {
            if (!et_username.text.isEmpty() && !et_password.text.isEmpty()) {
                hideKeyboard()
                postLogin(et_username.text.toString(), et_password.text.toString())
            } else
                Toast.makeText(this, "Kolom username dan password harus diisi", Toast.LENGTH_SHORT).show()
        }
        btn_forgot_password.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordAct::class.java))
        }
    }

    /***** API Connection *****/
    fun postLogin(username: String, password: String) {
        showLoading()
        launch {
            mApiService.postLogin(username, password).with(scheduler).subscribe(
                {
                    dismissLoading()
                    Hawk.put(User::class.java.name, it)
                    startActivity(Intent(this, HomeAct::class.java))
                    finish()
                },
                { err ->
                    dismissLoading()
                    Toast.makeText(this, "User tidak terdaftar", Toast.LENGTH_SHORT).show()
                })
        }
    }
}
