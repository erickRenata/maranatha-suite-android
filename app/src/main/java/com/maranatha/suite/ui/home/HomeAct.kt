package com.maranatha.suite.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.maranatha.suite.R
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.Article
import com.maranatha.suite.model.response.User
import com.maranatha.suite.ui.article.ArticleAct
import com.maranatha.suite.ui.article.DetailArticleAct
import com.maranatha.suite.ui.donation.DonationAct
import com.maranatha.suite.ui.job.CategoryJobAct
import com.maranatha.suite.ui.login.LoginAct
import com.maranatha.suite.ui.notifications.NotificationsAct
import com.maranatha.suite.utils.with
import com.orhanobut.hawk.Hawk
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.*


class HomeAct : BaseActivity() {

    private var articleList: MutableList<Article> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initUserProfile()
        initOnClickListener()
        getArticleList()
    }

    private fun initUserProfile() {
        val user = Hawk.get<User>(User::class.java.name)
        tv_user_name.text = user.Nama
        tv_user_faculty.text = user.Fakultas
        val imageUrl = user.ImageURL
        if(!imageUrl!!.isEmpty()){
            Picasso
                .get()// give it the context
                .load(user.ImageURL) // load the image
                .into(iv_user)
        }
    }

    private fun initListArticle() {

        lin_article.removeAllViews()
        for (item in articleList) {

            var v: View?

            v = layoutInflater.inflate(R.layout.item_news, null)
            val tvName = v!!.findViewById(R.id.tv_article_title) as TextView
            val ivClose = v.findViewById(R.id.iv_article) as ImageView

            tvName.text = item.Title
            Picasso
                .get()// give it the context
                .load(item.ImageURL) // load the image
                .into(ivClose)

            v.setOnClickListener {
                val intent = Intent(this, DetailArticleAct::class.java)
                intent.putExtra(Article::class.java.name, item)
                startActivity(intent)
            }

            lin_article.addView(v)

        }

    }

    private fun initOnClickListener() {
        lin_nav_alumni.setOnClickListener {
            Toast.makeText(this, "This feature is Coming Soon", Toast.LENGTH_SHORT).show()
        }
        lin_nav_article.setOnClickListener {
            startActivity(Intent(this, ArticleAct::class.java))
        }
        lin_nav_job_portal.setOnClickListener {
            startActivity(Intent(this, CategoryJobAct::class.java))
        }
        lin_nav_donation.setOnClickListener {
            startActivity(Intent(this, DonationAct::class.java))
        }

        iv_notifications.setOnClickListener {
            startActivity(Intent(this, NotificationsAct::class.java))
        }
        iv_sign_out.setOnClickListener {
            AlertDialog.Builder(this)
                    .setTitle(getString(R.string.dialog_title_logout_text))
                    .setMessage(getString(R.string.dialog_desc_logout_text))
                    .setPositiveButton(R.string.dialog_option_yes_text) { dialog, which ->
                        Hawk.delete(User::class.java.name)
                        startActivity(
                                Intent(this, LoginAct::class.java)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        )
                        finish()
                    }
                    .setNegativeButton(getString(R.string.dialog_option_no_text), null)
                    .create()
                    .show()
        }

    }

//    private fun setupViewPager() {

//        val adapter = ArticleItemFragment.MyFragmentPagerAdapter(getSupportFragmentManager())
//
//        var firstFragmet: ArticleItemFragment = ArticleItemFragment.newInstance("First Fragment")
//        var secondFragmet: ArticleItemFragment = ArticleItemFragment.newInstance("Second Fragment")
//        var thirdFragmet: ArticleItemFragment = ArticleItemFragment.newInstance("Third Fragment")
//
//        adapter.addFragment(firstFragmet, "ONE")
//        adapter.addFragment(secondFragmet, "TWO")
//        adapter.addFragment(thirdFragmet, "THREE")
//
//        vp_article!!.adapter = adapter

//        tabs!!.setupWithViewPager(viewpager)

    //    }

    /***** API Connection *****/
    fun getArticleList() {
//        showLoading()
        launch {
            mApiService.getArticleList().with(scheduler).subscribe(
                {
//                    dismissLoading()
                    articleList.clear()
                    articleList.addAll(it)
                    initListArticle()
                },
                { err ->
//                    dismissLoading()
                    Toast.makeText(this, "Oops! Something error ".plus(err), Toast.LENGTH_SHORT).show()
                })
        }
    }
}
