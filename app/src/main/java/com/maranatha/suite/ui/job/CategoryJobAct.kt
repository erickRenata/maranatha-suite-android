package com.maranatha.suite.ui.job

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.maranatha.suite.R
import com.maranatha.suite.adapter.JobCategoryAdapter
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.JobCategory
import com.maranatha.suite.utils.with
import kotlinx.android.synthetic.main.activity_donation.*

class CategoryJobAct : BaseActivity() {

    lateinit var donationAdapter: JobCategoryAdapter
    private var jobCategoryList: MutableList<JobCategory> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_list)

        getJobCategories()
        initOnClickListener()
    }

    private fun initAdapter() {

        donationAdapter = JobCategoryAdapter(this, jobCategoryList, { handleClick(it) })
        rv_content.layoutManager = GridLayoutManager(this, 2)
        rv_content.adapter = donationAdapter
    }

    /***** Implement Method *****/
    fun handleClick(position: Int) {
        startActivity(Intent(this, JobAct::class.java)
                .putExtra(JobCategory::class.java.name, jobCategoryList[position]))
    }

    private fun initOnClickListener() {
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    /***** API Connection *****/
    fun getJobCategories() {
        showLoading()
        launch {
            mApiService.getJobCategories().with(scheduler).subscribe(
                    {
                        dismissLoading()
                        jobCategoryList.clear()
                        jobCategoryList.addAll(it)
                        initAdapter()
                    },
                    { err ->
                        dismissLoading()
                        Toast.makeText(this, "Oops! Something error ".plus(err), Toast.LENGTH_SHORT).show()
                    })
        }
    }
}
