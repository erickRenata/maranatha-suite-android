package com.maranatha.suite.ui.article

import android.os.Bundle
import com.maranatha.suite.R
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.Article
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_article.*

class DetailArticleAct : BaseActivity() {

    lateinit var detailArticle: Article

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_article)

        detailArticle = intent.getParcelableExtra(Article::class.java.name)
        initView()
        initOnClickListener()
    }

    private fun initView() {
        tv_article_date.text = detailArticle.Datepublished
        tv_article_title.text = detailArticle.Title
        tv_article_content.text = detailArticle.Content
        tv_article_author.text = detailArticle.Author
        Picasso
            .get()// give it the context
            .load(detailArticle.ImageURL) // load the image
            .into(iv_article)
    }

    private fun initOnClickListener() {
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }
}
