package com.maranatha.suite.ui.article

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.maranatha.suite.R
import com.maranatha.suite.adapter.ArticleAdapter
import com.maranatha.suite.adapter.JobAdapter
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.Article
import com.maranatha.suite.model.response.Job
import com.maranatha.suite.utils.with
import kotlinx.android.synthetic.main.activity_donation.*

class ArticleAct : BaseActivity() {

    lateinit var articleAdapter: ArticleAdapter
    private var articleList: MutableList<Article> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

//        initAdapter()
        initOnClickListener()
        getArticleList()
    }

    private fun initAdapter() {
        articleAdapter = ArticleAdapter(this, articleList, { handleClick(it) })
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = articleAdapter
    }

    /***** Implement Method *****/
    fun handleClick(position: Int) {
        val intent = Intent(this, DetailArticleAct::class.java)
        intent.putExtra(Article::class.java.name, articleList[position])
        startActivity(intent)
    }

    private fun initOnClickListener() {
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    /***** API Connection *****/
    fun getArticleList() {
        showLoading()
        launch {
            mApiService.getArticleList().with(scheduler).subscribe(
                    {
                        dismissLoading()
                        articleList.clear()
                        articleList.addAll(it)
                        initAdapter()
                    },
                    { err ->
                        dismissLoading()
                        Toast.makeText(this, "Oops! Something error ".plus(err), Toast.LENGTH_SHORT).show()
                    })
        }
    }
}
