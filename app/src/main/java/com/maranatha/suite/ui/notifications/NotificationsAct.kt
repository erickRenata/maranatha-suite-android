package com.maranatha.suite.ui.notifications

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.maranatha.suite.R
import com.maranatha.suite.adapter.NotificationsAdapter
import com.maranatha.suite.base.BaseActivity
import com.maranatha.suite.model.response.Notifications
import com.maranatha.suite.utils.with
import kotlinx.android.synthetic.main.activity_notifications.*

class  NotificationsAct : BaseActivity() {

    lateinit var notificationsAdapter: NotificationsAdapter
    private var notificationsList: MutableList<Notifications> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)

        getNotifications()
        initOnClickListener()
    }

    private fun initAdapter() {

        notificationsAdapter = NotificationsAdapter(this, notificationsList, { handleClick(it) })
        rv_content.layoutManager = LinearLayoutManager(this)
        rv_content.adapter = notificationsAdapter
    }

    /***** Implement Method *****/
    fun handleClick(position: Int) {
    }


    private fun initOnClickListener() {
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    /***** API Connection *****/
    fun getNotifications() {
        showLoading()
        launch {
            mApiService.getNotifications().with(scheduler).subscribe(
                    {
                        dismissLoading()
                        notificationsList.clear()
                        notificationsList.addAll(it)
                        initAdapter()
                    },
                    { err ->
                        dismissLoading()
                        Toast.makeText(this, "Oops! Something error ".plus(err), Toast.LENGTH_SHORT).show()
                    })
        }
    }
}
