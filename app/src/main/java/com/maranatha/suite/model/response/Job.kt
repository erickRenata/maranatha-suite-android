package com.maranatha.suite.model.response

data class Job(
    val Address: String,
    val Categories: JobCategory,
    val CategoriesId: String,
    val Company: String,
    val Description: String,
    val Icon_Company: String,
    val Id: String,
    val Location: String,
    val Name: String,
    val Position: String,
    val Poster: User,
    val PosterId: String,
    val Skill_Requirements: String
)