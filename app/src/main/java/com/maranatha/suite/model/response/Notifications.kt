package com.maranatha.suite.model.response

data class Notifications(
    val Content: String,
    val CreatedAt: String,
    val Guid: String,
    val Is_New: Boolean,
    val User: User,
    val UserId: String
)