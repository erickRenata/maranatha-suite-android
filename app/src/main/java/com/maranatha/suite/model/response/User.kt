package com.maranatha.suite.model.response

data class User(
    val Alamat: String,
    val AlamatSekolah: String,
    val Angkatan: Int,
    val Email: String,
    val Fakultas: String,
    val IPK: String,
    val Id: String,
    val IkutWisuda: String,
    val ImageURL: String?,
    val JenisKelamin: String,
    val Jurusan: String,
    val KodePos: String,
    val Kota: String,
    val KotaSekolah: String,
    val NRP: String,
    val Nama: String,
    val NoHP: String,
    val NoTelepon: String,
    val Password: String,
    val Pembayaran: String,
    val Program: String,
    val Provinsi: String,
    val SekolahAsal: String,
    val TanggalDaftar: String,
    val TanggalLahir: String,
    val TanggalLulus: String,
    val TempatLahir: String,
    val UkuranToga: String,
    val Username: String
)