package com.maranatha.suite.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class JobCategory(
    val Available_Jobs: Int = 0,
    val CategoriesName: String?,
    val Icon_Category: String?,
    val Id: String = ""
) : Parcelable