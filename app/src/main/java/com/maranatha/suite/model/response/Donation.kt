package com.maranatha.suite.model.response

data class Donation(
    val CreatedAt: String,
    val Description: String,
    val Id: String,
    val Image_Donation: String,
    val Money: Double,
    val Title: String,
    val User: User,
    val UserId: String
)