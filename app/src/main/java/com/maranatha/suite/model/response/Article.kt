package com.maranatha.suite.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Article(
    val Author: String = "",
    val Content: String = "",
    val Datepublished: String = "",
    val Id: String = "",
    val ImageURL: String = "",
    val Title: String = ""
) : Parcelable